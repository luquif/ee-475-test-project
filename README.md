# Setting up Intellij and GitLab

This will use a tutorial from [CSE 373](https://courses.cs.washington.edu/courses/cse373/20au/projects/cse143review/setup/) for the most part. From this tutorial, you will follow all the steps. I list them below for clarity:
1. Install Java JDK 14. From my understanding, this enables or enchances the use of Java in Intellij. 
2. Install Git. (Not to be confused with GitLab or GitHub.)
3. Get a GitLab account. I will add you to this test project.
4. SSH Key Pair. 
    - Follow the steps under 'Generate a New SSH Key Pair.' 
    - Follow the first step under 'Add the SSH Key to Your GitLab Account,' then click your icon in GitLab in the upper right corner > Settings > SSH Keys (on the left side options) > Paste the SSH Key (starting with 'ssh-rsa ....') > Add Key
5. Download Intellij
6. Connect to the repository
7. Walk through 'Sanity Check Java Version', 'Configure Checkstyle', 'Configure Generics Checks' together to be on same page. However, I am not sure how important this part is. 


# Running through Intellij and GitLab Basics
## IntelliJ
- Navigating through the files in Intellij
    - [Picture](https://courses.cs.washington.edu/courses/cse373/20au/projects/cse143review/images/project-initial.png) of Intellij Layout

## GitLab
GitLab enables teams to work on the same files and projects in an efficient manner. This is compared to having to making changes to file and sending via slack/email, back and forth for every change.
The way I simplify it down is there is a version of files called 'main' that has working files or the most up-to-date files. You will have access to this once you connect IntelliJ and GitLab.

- Pull, Commit, and Pushing
    - When you make a change to the files or folders that you are certain of and want to share, you commit and push. This updates the directory on GitLab which everyone has access to. Until then, the changes will remain only on your computer.
    - Since others are working on the files as well, you may want to work on what Joe did earlier, so you pull to your computer.
- Creating a branch and merging
    - main ideally is the version of the projects files that is working or verified. Thus, branching is essentially creating a copy of main for you to make changes in. Once you feel good and want some to verify, you commit and push. On GitLab, once someone has looked through your changes, they can verify the changes and add them to main.
- Using '.md' files to journal and keep track of progress?
